# CRUD de Usuários

Projeto front-end em React JS e simulação de back-end Json-server. Realiza crud (create/read/update/delete) de usuários.

## Back-end Json Server
 - Cria package.json
 `npm init -y`
 - Instala dependência json-server
 `npm i --save json-server@0.13.0 -E`
 - Em package.json adicionar script:
 ```javascript
 "scripts": {
    "start": "json-server --watch db.json --port 3001"
  }
 ```
 
## Front-end React JS
 - Criação:
 `npx create-react-app frontend`
 - Adicionando dependências:
    - `npm install axios`
    - `npm install bootstrap`
    - `npm install --save react-router`
    - `npm install --save react-router-dom`
    - Insira`"font-awesome": "4.7.0"` em package.json > dependencies
 - Para disponibilizar acesso à aplicação na rede adicione o script ao package.json e execute o comando `npm start network`
 ```javascript
 "scripts": {
    "start network": "HOST=0.0.0.0 react-scripts start"
  }
 ```

## Prewiew
 ![App Users](frontend/src/assets/react-crud-preview.gif)