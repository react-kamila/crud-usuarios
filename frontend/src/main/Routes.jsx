import React from 'react'
import { Switch, Route, Redirect } from 'react-router';

import Home from '../components/home/Home';
import UserCrud from '../components/user/UserCrud';

export default props =>
<Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/users" component={UserCrud} /> {/** Qlqr coisa q comece em 'users' vem pra essa rota, pois não tem exact*/}
    <Redirect from="*" to="/" />
</Switch>