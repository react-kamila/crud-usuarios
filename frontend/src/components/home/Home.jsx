import React from 'react';
import Main from '../template/Main';

export default props =>

    <Main icon="home" title="Início"
        subtitle="Projeto de cadastro de usuários em React JS.">
        <div className="display-4">Bem vindo!</div>
        <hr />
        <p className="mb-0">Sistema de cadastro desenvolvido em React JS</p>
    </Main>