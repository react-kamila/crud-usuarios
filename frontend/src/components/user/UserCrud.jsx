import React, { Component } from 'react'
import Main from '../template/Main'
import axios from 'axios';

const headerProps = {
    icon: 'users',
    title: 'Usuários',
    subtitle: 'Cadastro de usuários: Incluir, Listar, Altera e Excluir'
}

const baseUrl = 'http://10.0.2.192:3001/users';
const initialState = {
    user: { name: '', email: '' },
    list: []
}

export default class UserCrud extends Component {

    state = { ...initialState };

    componentWillMount() { // ciclo de vida, chamada antes de carregar a página
        axios(baseUrl).then(resp => {
            this.setState({ list: resp.data })
        });
    }

    clear() {
        this.setState({ user: initialState.user });
    }

    save() { // e update
        const user = this.state.user;
        /* Para alterar um objeto é mais interessante criar um clone do mesmo
         * Na linha acima estamos apenas referenciando o objeto state.user pois não vamos alterá-lo
         */
        const method = user.id ? 'put' : 'post'; // se tiver o atributo 'id' deve ser feito update
        const url = user.id ? `${baseUrl}/${user.id}` : `${baseUrl}`;
        axios[method](url, user)
            .then(resp => {
                const list = this.getUpdatedList(resp.data);
                this.setState({ user: initialState.user, list })
            });
    }

    getUpdatedList(user, add = true) {
        const list = this.state.list.filter(u => u.id !== user.id); // retira da lista user recebido como parâmetro
        if (add) list.unshift(user); // se parâmetro add==true adiciona user na primeira posição da lista
        return list;
    }

    updateField(event) {
        console.log('digitando');
        const user = { ...this.state.user };
        user[event.target.name] = event.target.value;
        this.setState({ user }); // mesmo nome pois a chave tem o mesmo nome da const (EcmaScript 2015)
    }

    renderForm() {
        return (
            <div className="form">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Nome</label>
                            <input type="text" className="form-control"
                                name="name"
                                value={this.state.user.name}
                                onChange={e => this.updateField(e)}
                                placeholder="Nome do usuário" />
                        </div>
                    </div>

                    <div className="col-12 col-md-6">
                        <div className="form-group">
                            <label>Email</label>
                            <input type="text" className="form-control"
                                name="email"
                                value={this.state.user.email}
                                onChange={e => this.updateField(e)}
                                placeholder="E-mail" />
                        </div>
                    </div>                   
                </div>
                <hr />

                <div className="row">
                    <div className="col-12 d-flex justify-content-end">
                        <button className="btn btn-primary" onClick={e => this.save(e)}>Salvar</button>
                        <button className="btn btn-secondary ml-2" onClick={e => this.clear(e)}>Cancelar</button>
                    </div>
                </div>
            </div>
        )
    }

    /* Carrega usuário para edição */
    load(user) {
        this.setState({ user });
    }

    remove(user) {
        axios.delete(`${baseUrl}/${user.id}`).then(resp => {
            const list = this.getUpdatedList(user, false);
            this.setState({ list });
        })
    }

    /* Tabela que lista usuários salvos */
    renderTable() {
        return (
          <div className="table-responsive">
            <table className="table mt-4">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>Ações</th>
                </tr>
              </thead>

              <tbody>{this.renderRows()}</tbody>
            </table>
          </div>
        );
    }

    renderRows() {
        return this.state.list.map(user => {
            return (
              <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>
                  <div className="row m-0">
                    <div className="col">
                      <button className="btn btn-warning" onClick={() => this.load(user)}>
                        <i className="fa fa-pencil"></i>
                      </button>
                    </div>
                    <div className="col">
                      <button className="btn btn-danger ml-2" onClick={() => this.remove(user)}>
                        <i className="fa fa-trash"></i>
                      </button>
                    </div>
                  </div>
                </td>
              </tr>
            );
        })
    }

    render() {
        console.log(this.state.list)
        return (
            
            <Main {...headerProps}>
                {this.renderForm()}
                {this.renderTable()}
            </Main>
        )
    }
}
